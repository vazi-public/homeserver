## Generation certificat auto-signé

Generation d'une clef privée:
```
openssl genrsa -out home.key 2048
```

Generation de la demande de signature de certificat:
```
openssl req -new -newkey rsa:2048 -nodes -out home.csr -keyout home.key -subj "/C=FR/ST=France/CN=home" 
```

Generation du certificat:
```
openssl x509 -req -days 3650 -in home.csr -signkey home.key -out home.crt -extfile <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS.1:*.home,DNS.2:plex,DNS.3:homeassistant")) -extensions SAN
```
