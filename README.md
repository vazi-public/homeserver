## Pré-requis

Installer les composants suivants:
- docker
- docker-compose 

Configurer une résolution DNS locale en modifiant le fichier /etc/hosts:
```
127.0.0.1 plex homeassistant
```

## Configuration

Pour adapter l'execution du cluster il est nécessaire de créer un fichier d'environnement `.env` avec les variables de configuration du fichier utilisées. Par exemple:
```
# Plex
PLEX_CLAIM=<claim-token>
PLEX_ADVERTISE_IP=http://127.0.0.1:32400/
```

## Execution

Lancer le compose:
```
docker-compose up -d 
```

Enjoy:
```
https://plex
https://homeassistant
```
